$(document).ready(function(){
    $("#title-links").on("click","a", function (event) {
        event.preventDefault();
        var id  = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({scrollTop: top}, 500);
    });
});
$(function () {
    $('#m-btn-1').click(function () {
      $('.modal').addClass('modal_active');
    });
   
    $('.modal__close-button').click(function () {
      $('.modal').removeClass('modal_active');
    });
  });